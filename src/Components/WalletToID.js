import React from 'react';
import axios from 'axios';
import { Button, Jumbotron, FormControl } from "react-bootstrap";

class WalletToID extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            id: "",
            wallet: "",
            status:""
        
        }
    }

    handleIDChange = (e) => {
        this.setState({id:e.target.value});
    }

    handleWalletChange = (e) => {
        this.setState({wallet:e.target.value});
    }

    submit = (e) => {
        e.preventDefault();

        axios.get(`http://matershev.com:8080/mapWalletToPerson?ethWallet=${this.state.wallet}&pID=${this.state.id}`)
        .then((response) => {
            console.log(response);
            this.setState({status:"Mapped!!"})
        })
        .catch((error) => {
            // handle error
            console.log(error);
        })

    }

    render(){
        return(<div>
                <Jumbotron style={{ padding: "40px", margin: "10% auto", width: "400px", height: "600px", display: "flex", justifyContent: "space-around", flexDirection: "column" }}>
                <h3>Map Wallet to ID</h3>
                    <div>
                        <label>Wallet Address</label>
                        <FormControl
                            type="text"
                            value={this.state.wallet}
                            placeholder="Enter wallet address..."
                            onChange={this.handleWalletChange}
                        />
                    </div>
                    <div>
                        <label>ID</label>
                        <FormControl
                            type="text"
                            value={this.state.id}
                            placeholder="Enter ID..."
                            onChange={this.handleIDChange}
                        />
                    </div>
                    <Button bsStyle="primary" onClick={this.submit}>MAP!</Button>
                    <div>{`Status: ${this.state.status}`}</div>

                </Jumbotron>
            </div>);
    }   
}

export default WalletToID;