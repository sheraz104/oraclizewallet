import React from 'react';
import axios from 'axios';
import { Button, Jumbotron, FormControl } from "react-bootstrap";

class AddPerson extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            firstName: "",
            lastName: "",
            id:""
        }
    }

    handleFirstNameChange = (e) => {
        this.setState({firstName:e.target.value});
    }

    handleLastNameChange = (e) => {
        this.setState({lastName:e.target.value});
    }

    submit = (e) => {
        e.preventDefault();

        axios.get(`http://matershev.com:8080/addPerson?firstName=${this.state.firstName}&lastName=${this.state.lastName}`)
        .then((response) => {
            console.log(response);
            this.setState({id:response.data})
        })
        .catch((error) => {
            // handle error
            console.log(error);
        })

    }

    render(){
        return(<div>
                <Jumbotron style={{ padding: "40px", margin: "10% auto", width: "400px", height: "600px", display: "flex", justifyContent: "space-around", flexDirection: "column" }}>
                <h3>Add Person</h3>
                    <div>
                        <label>First Name</label>
                        <FormControl
                            type="text"
                            value={this.state.firstName}
                            placeholder="Enter First Name..."
                            onChange={this.handleFirstNameChange}
                        />
                    </div>
                    <div>
                        <label>Last Name</label>
                        <FormControl
                            type="text"
                            value={this.state.lastName}
                            placeholder="Enter Last Name..."
                            onChange={this.handleLastNameChange}
                        />
                    </div>
                    <Button bsStyle="primary" onClick={this.submit}>ADD!</Button>
                    <div>{`ID: ${this.state.id}`}</div>

                </Jumbotron>
            </div>);
    }   
}

export default AddPerson;