import React, { Component } from 'react';
import { Button, Jumbotron, FormControl } from "react-bootstrap";
import AsyncButton from 'react-async-button';

import contractABI from "./ABI_ERC20";
import Status from "./Status";
const Web3 = require('web3')
const WalletProvider = require('truffle-hdwallet-provider-privkey')
          

class SendToken extends Component {
    constructor(props) {
        super(props);
        this.state = {
            privKey: "",
            tokenAmount: "",
            toAddress: "",
            fromAddress:"",
            amount:"",
            status:"Status"
        }
    }

    handlePrivKeyChange = (e) => {
        this.setState({ privKey: e.target.value })
    }
    handleContractAddressChange = (e) => {
        this.setState({ contractAddress: e.target.value })
    }
    handleTokenAmoundChange = (e) => {
        this.setState({ tokenAmount: e.target.value })
    }
    handleToAddressChange = (e) => {
        this.setState({ toAddress: e.target.value })
    }
    sendToken = (e) => {
        e.preventDefault();
             return new Promise((resolve, reject) => {
            const{ tokenAmount } = this.state;
            const w = new WalletProvider(this.state.privKey.trim(), "https://ropsten.infura.io/QWMgExFuGzhpu2jUr6Pq")

            
            const web3 = new Web3(w.engine)
            web3.eth.getAccounts(async (err, accounts) => {
                if (err) {
                    throw err;
                }
                try{
                const contractInstance = web3.eth.contract(contractABI.ABI).at("0x63e06cfceac7a641db1988f48f7bc1b329c40642");
                // instantiate by address

                var TransferEvent = contractInstance.Transfer({}, {fromBlock: 'latest'});
                TransferEvent.watch((error, result) => {
                    console.log("inside transfer", result)

                    let {from, to, value} = result.args;
                    value = value.toString().toLowerCase();
                    to = to.toString().toLowerCase();
                    from = from.toString().toLowerCase();

                    let {fromAddress, toAddress, amount} = this.state;
                    fromAddress = fromAddress.toLowerCase();
                    toAddress = toAddress.toLowerCase();
                    amount = amount.toLowerCase();
                    
                    console.log(fromAddress, toAddress, amount)
                    console.log(from, to, value)
                    if(from === fromAddress){
                        if(to === toAddress){
                            if(value === amount){
                                this.setState({status:"Transfer is successfull"})
                                this.setState({privKey:"",contractAddress:"",toAddress:"",tokenAmount:""})

                                resolve();
                            }
                        }
                    }
                })

                var UnsuccessfulTransfer = contractInstance.TranferUnsuccessful({}, {fromBlock: 'latest'});
                UnsuccessfulTransfer.watch((error, result) => {
                    console.log("inside unsuccessful")
                    let {from, to, value} = result.returnValues;
                    value = value.toString().toLowerCase();
                    to = to.toString().toLowerCase();
                    from = from.toString().toLowerCase();

                    let {fromAddress, toAddress, amount} = this.state;

                    fromAddress = fromAddress.toLowerCase();
                    toAddress = toAddress.toLowerCase();
                    amount = amount.toLowerCase();

                    if(from === fromAddress){
                        if(to === toAddress){
                            if(value === amount){
                                this.setState({status:"Transfer is unsuccessful: You are not authorized to make trannsaction"})
                                reject();
                            }
                        }
                    }
                })

                this.setState({fromAddress:accounts[0]});

                let totalAmount;
                if (tokenAmount.includes(".")) {
                const amountArray = tokenAmount.split(".");
                const amountDecimalPart = amountArray[1];
                const zeros = 18 - amountDecimalPart.length;
                totalAmount = amountArray[0] + amountDecimalPart + "0".repeat(zeros);
                } else {
                totalAmount = tokenAmount + "0".repeat(18);
                }


                this.setState({amount:totalAmount});

                contractInstance.transfer(this.state.toAddress.trim(), totalAmount, {from: accounts[0], gas: "2100000"}, (err, hash) => {
                    console.log("mined");
                    this.setState({status:"Pending..."})
                })
                
                } catch(e){
                    reject(e);
                }

            }
            )
        })


    }
    render() {
        return (
            <div>
                <Jumbotron style={{ padding: "40px", margin: "10% auto", width: "400px", height: "600px", display: "flex", justifyContent: "space-around", flexDirection: "column" }}>
                    <h3>Send Tokens</h3>
                    <div>
                        <label>Private Key</label>
                        <FormControl
                            type="text"
                            value={this.state.privKey}
                            placeholder="Enter Your private key..."
                            onChange={this.handlePrivKeyChange}
                        />
                    </div>

                    <div>
                        <label>Address to send Tokens to</label>

                        <FormControl
                            type="text"
                            value={this.state.toAddress}
                            placeholder="Enter the Address to send tokens to..."
                            onChange={this.handleToAddressChange}
                        />
                    </div>
                    <div>
                        <label>Token Amount to send</label>

                        <FormControl
                            type="text"
                            value={this.state.tokenAmount}
                            placeholder="Enter Token Amount..."
                            onChange={this.handleTokenAmoundChange}
                        />
                    </div>


                    <AsyncButton
                        className="btn"
                        text="Transfer"
                        pendingText="Tranfering..."
                        fulFilledText="Successfull!"
                        rejectedText="Failed! Try Again"
                        loadingClass="isSaving"
                        fulFilledClass="btn-primary"
                        rejectedClass="btn-danger"
                        onClick={this.sendToken}
                    />
                    <Status status ={this.state.status} />
                    </Jumbotron>
            </div>
        );
    }
}

export default SendToken;