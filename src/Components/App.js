import React, { Component } from 'react';
import SendToken from './SendToken'
import WalletFunctions from "./WalletFunctions";
import AddPerson from "./AddPerson";
import WalletToID from "./WalletToID";
import GetWallet from "./GetWallet";

class App extends Component {
  render() {
    return (
      <div style={{ textAlign: "center", display:'flex', flexDirection:"column" }}>
        <h1>Token Wallet</h1>
        <div className="mainContent" style={{ display: 'flex', justifyContent: "space-around" }}>
          <SendToken />
          <AddPerson />
          <WalletToID />
          <GetWallet />
          {/* <WalletFunctions /> */}
        </div>
      </div>
    );
  }
}

export default App;
