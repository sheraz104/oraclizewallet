import React from 'react';
import axios from 'axios';
import { Button, Jumbotron, FormControl } from "react-bootstrap";

class GetWallet extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            wallet: "",
            firstName:"",
            lastName:"",
            id:""
        
        }
    }


    handleWalletChange = (e) => {
        this.setState({wallet:e.target.value});
    }

    submit = (e) => {
        e.preventDefault();

        axios.get(`http://matershev.com:8080/getWalletMapping?ethWallet=${this.state.wallet}`)
        .then((response) => {
            console.log(response);
            const {firstName, lastName, id} = response.data.result;
            this.setState({firstName, lastName, id});
        })
        .catch((error) => {
            // handle error
            console.log(error);
        })

    }

    render(){
        return(<div>
                <Jumbotron style={{ padding: "40px", margin: "10% auto", width: "400px", height: "600px", display: "flex", justifyContent: "space-around", flexDirection: "column" }}>
                <h3>Get Wallet</h3>
                    <div>
                        <label>Wallet Address</label>
                        <FormControl
                            type="text"
                            value={this.state.wallet}
                            placeholder="Enter wallet address..."
                            onChange={this.handleWalletChange}
                        />
                    </div>
                
                    <Button bsStyle="primary" onClick={this.submit}>GET!</Button>
                    {this.state.firstName && <Info firstName={this.state.firstName} lastName={this.state.lastName} id={this.state.id} />}

                </Jumbotron>
            </div>);
    }   
}

const Info = (props) => {
    return (<div> 
        <p>FirstName: {props.firstName}</p>
        <p>Last Name: {props.lastName}</p>
        <p>ID: {props.id}</p>
    </div>);
}
export default GetWallet;