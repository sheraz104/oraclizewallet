import React, { Component } from 'react';
import { Button, Jumbotron, FormControl } from "react-bootstrap";
import contractABI from "./ABI_ERC20";
const Web3 = require('web3')
const WalletProvider = require('truffle-hdwallet-provider-privkey')


class WalletFunctions extends Component {
    constructor(props) {
        super(props);
        this.state = {
            privKey: "",
            contractAddress: "",
            tokenBalance: 0,
            publicKey:""
        }
    }
    handlePrivKeyChange = (e) => {
        this.setState({ privKey: e.target.value })
    }
    handlePublicKeyChange = (e) => {
        this.setState({ publicKey: e.target.value })
    }
    handleContractAddressChange = (e) => {
        this.setState({ contractAddress: e.target.value })
    }
    checkBalance = (e) => {
        e.preventDefault();
        let web3;
        if(this.state.privKey){
        const w = new WalletProvider(this.state.privKey.trim(), "https://ropsten.infura.io/QWMgExFuGzhpu2jUr6Pq");
        web3 = new Web3(w.engine)
        } else {
            web3 = new Web3(new Web3.providers.HttpProvider("https://ropsten.infura.io/QWMgExFuGzhpu2jUr6Pq"))
        }
        
        web3.eth.getAccounts(async (err, accounts) => {
            if (err) {
                throw err;
            }
            const contractInstance = new web3.eth.Contract(contractABI.ABI,this.state.contractAddress.trim());
            // instantiate by address
            const decimals = await contractInstance.methods.decimals().call({from:accounts[0]});
            let balanceAccount = this.state.publicKey ? this.state.publicKey : accounts[0];
            let balance = await contractInstance.methods.balanceOf(balanceAccount).call({from:balanceAccount})
            this.setState({ tokenBalance: balance / 10 ** decimals });
            this.setState({privKey:"",contractAddress:"",publicKey:""});

        }
        )
    }
    render() {
        return (
            <div>
                <Jumbotron style={{ padding: "40px", margin: "10% auto", width: "400px", height: "600px", display: "flex", justifyContent: "space-around", flexDirection: "column" }}>
                <h3>Check Token Balance</h3>
                    <div>
                        <label>Private Key</label>
                        <FormControl
                            type="text"
                            value={this.state.privKey}
                            placeholder="Enter your private key..."
                            onChange={this.handlePrivKeyChange}
                        />
                    </div>
                    <div>OR</div>
                    <div>
                        <label>Public Key</label>
                        <FormControl
                            type="text"
                            value={this.state.publicKey}
                            placeholder="Enter your private key..."
                            onChange={this.handlePublicKeyChange}
                        />
                    </div>
                    <div>
                        <label>Token Address</label>

                        <FormControl
                            type="text"
                            value={this.state.contractAddress}
                            placeholder="Enter the token Address..."
                            onChange={this.handleContractAddressChange}
                        />
                    </div>
                    <Button bsStyle="primary" onClick={this.checkBalance}>Check Balance</Button>
                    <div>{`Balance: ${this.state.tokenBalance}`}</div>

                </Jumbotron>
            </div>
        )
    }
}

export default WalletFunctions;